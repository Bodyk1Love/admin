import * as React from 'react';

import { Admin, Resource, ListGuesser, EditGuesser}  from 'react-admin'
import { Layout } from './layout';
import dataProvider from './dataProvider';
import LoginPage from './components/LoginPage';
import Dashboard from './components/Dashboard';
import polyglotI18nProvider from 'ra-i18n-polyglot';
import englishMessages from './i18n/en';

import bot from './components/bot';
import product from './components/product';
import field from './components/field';
import clients from './components/clients';
import orders from './components/orders';


const i18nProvider = polyglotI18nProvider(locale => {
  return englishMessages;
}, 'en');


const App = () => {

    let authProvider = {
      login: (user) => {
        localStorage.setItem('user', JSON.stringify(user));
        return Promise.resolve();
      },
      logout: () => {
          localStorage.removeItem('user');
          return Promise.resolve();
      },
      checkError: () => Promise.resolve(),
      checkAuth: () => {
        let user_str = localStorage.getItem('user')
        if (user_str) {
          let user = JSON.parse(user_str)
          return user['hash'] ? Promise.resolve() : Promise.reject()
        } else {
          return Promise.reject()
        }
      },
      getPermissions: () => Promise.reject('Unknown method'),
      getIdentity: () => {
        let user_str = localStorage.getItem('user')
        let user = user_str ? JSON.parse(user_str) : {}
        return Promise.resolve({
            id: user['id'],
            fullName: user['first_name']
        })
      }
    }

    return (
        <Admin 
            dataProvider={dataProvider}
            authProvider={authProvider}
            loginPage={LoginPage}
            layout={Layout}
            dashboard={Dashboard}
            i18nProvider={i18nProvider}
        >
            <Resource 
                name="bots"
                {...bot}
            />
            <Resource 
                name="products" 
                {...product}
            />
            <Resource 
                name="fields" 
                {...field}
            />
            <Resource 
                name="clients" 
                {...clients}
            />
            <Resource 
                name="orders" 
                {...orders}
            />
        </Admin>
    )
};

export default App;