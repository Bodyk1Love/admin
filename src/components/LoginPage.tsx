import * as React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { withTypes } from 'react-final-form';
import TelegramLoginButton from 'react-telegram-login';
import { CircleLoading } from 'react-loadingg';
import {
    Avatar,
    Card,
} from '@material-ui/core';
import { createMuiTheme, makeStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import LockIcon from '@material-ui/icons/Lock';
import { Notification, useLogin, defaultTheme } from 'react-admin';


const useStyles = makeStyles(theme => ({
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        alignItems: 'center',
        justifyContent: 'flex-start',
        background: 'url(https://source.unsplash.com/collection/388793/1600x900)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    card: {
        minWidth: 300,
        marginTop: '6em',
    },
    avatar: {
        margin: '1em',
        display: 'flex',
        justifyContent: 'center',
    },
    icon: {
        backgroundColor: theme.palette.secondary.main,
    },
    hint: {
        marginTop: '1em',
        display: 'flex',
        justifyContent: 'center',
        color: theme.palette.grey[500],
    },
    form: {
        padding: '0 1em 1em 1em',
    },
    input: {
        marginTop: '1em',
    },
    actions: {
        padding: '0 1em 1em 1em',
    },
}));

interface FormValues {
    username?: string;
    password?: string;
}

const { Form } = withTypes<FormValues>();

const Login = () => {
    const [loading, setLoading] = useState(false);
    const classes = useStyles();
    const login = useLogin();

    const onTelegramAuth = (user) => {
      setLoading(true);
      login(user).catch((e) => {
        setLoading(false);
      });
    }

    return (
        <Form
            onSubmit={onTelegramAuth}
            render={() => (
                <form onSubmit={onTelegramAuth} noValidate>
                    <div className={classes.main}>
                        <Card className={classes.card}>
                            <div className={classes.avatar}>
                                <Avatar className={classes.icon}>
                                    <LockIcon />
                                </Avatar>
                            </div>
                            {
                              loading ? <CircleLoading /> : (
                                <div className={classes.form}>
                                  <TelegramLoginButton dataOnauth={onTelegramAuth} botName="sign_auth_bot" />
                                </div>
                              )
                            }
                        </Card>
                        <Notification />
                    </div>
                </form>
            )}
        />
    );
};

Login.propTypes = {
    authProvider: PropTypes.func,
    previousRoute: PropTypes.string,
};

const LoginWithTheme = (props: any) => (
    <ThemeProvider theme={createMuiTheme(defaultTheme)}>
        <Login {...props} />
    </ThemeProvider>
);

export default LoginWithTheme;
