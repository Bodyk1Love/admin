import * as React from "react";
import { Create, TextInput, SimpleForm, required } from 'react-admin';

const BotCreate = props => (
  <Create title="Створити бота" {...props}>
      <SimpleForm>
          <TextInput source="name" validate={required()}/>
          <TextInput source="token" validate={required()}/>
      </SimpleForm>
  </Create>
);

export default BotCreate;


