import * as React from "react";
import { Edit, TextInput, SimpleForm } from 'react-admin';

const BotEdit = props => (
  <Edit title="Редагувати бота" {...props}>
      <SimpleForm>
          <TextInput disabled source="id" />
          <TextInput source="name" />
          <TextInput source="token" />
      </SimpleForm>
  </Edit>
);

export default BotEdit;


