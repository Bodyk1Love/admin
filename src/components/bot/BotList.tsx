import * as React from "react";
import { List, Datagrid, TextField, EditButton } from 'react-admin';

const BotList = props => (
  <List {...props}>
      <Datagrid rowClick="edit">
          <TextField source="name" />
          <EditButton basePath="bots" />
      </Datagrid>
  </List>
);

export default BotList;


