import BotCreate from './BotCreate';
import BotList from './BotList';
import BotEdit from './BotEdit';
import Android from '@material-ui/icons/Android';

const resources = {
  icon: Android,
  create: BotCreate,
  list: BotList,
  edit: BotEdit
};

export default resources
