import * as React from "react";
import { 
  Create, 
  TextInput, 
  SimpleForm, 
  ReferenceInput, 
  required, 
  SelectInput
} from 'react-admin';

const ClientCreate = props => (
  <Create title="Створити поле" {...props}>
      <SimpleForm>
          <TextInput source="username" />
          <TextInput source="chat_id" />
          <ReferenceInput
              source="bot"
              reference="bots"
              validate={required()}
          >
            <SelectInput source="name" />
          </ReferenceInput>
      </SimpleForm>
  </Create>
);

export default ClientCreate;


