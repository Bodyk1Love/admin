import * as React from "react";
import { 
  Edit, 
  TextInput, 
  SimpleForm, 
  SelectInput,
  required,
  ReferenceInput
} from 'react-admin';

const ClientEdit = props => (
  <Edit title="Редагувати поле" {...props}>
      <SimpleForm>
          <TextInput disabled source="id" />
          <TextInput source="username" />
          <TextInput source="chat_id" />
          <ReferenceInput
              source="bot"
              reference="bots"
              validate={required()}
          >
            <SelectInput source="name" />
          </ReferenceInput>
      </SimpleForm>
  </Edit>
);

export default ClientEdit;


