import * as React from "react";
import { List, Datagrid, TextField, EditButton, ReferenceField } from 'react-admin';

const ClientList = props => (
  <List {...props}>
      <Datagrid rowClick="edit">
          <TextField source="username" />
          <ReferenceField source="bot" reference="bots" label="Бот" link="show">
            <TextField source="name" />
          </ReferenceField>
          <EditButton basePath="clients" />
      </Datagrid>
  </List>
);

export default ClientList;


