import ClientEdit from './ClientEdit';
import ClientList from './ClientList';
import ClientCreate from './ClientCreate';
import PersonIcon from '@material-ui/icons/Person';;

const resources = {
  icon: PersonIcon,
  create: ClientCreate,
  list: ClientList,
  edit: ClientEdit
};

export default resources
