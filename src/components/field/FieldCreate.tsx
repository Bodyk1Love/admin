import * as React from "react";
import { 
  Create, 
  TextInput, 
  SimpleForm, 
  ReferenceInput, 
  required, 
  SelectInput
} from 'react-admin';

const FieldCreate = props => (
  <Create title="Створити поле" {...props}>
      <SimpleForm>
          <TextInput source="title" />
          <SelectInput 
            source="type" 
            validate={required()}
            choices={[
              {id: "str", name: "Текстовий"},
              {id: "float", name: "Числовий"},
            ]}
          />
          <ReferenceInput
              source="bot"
              reference="bots"
              validate={required()}
          >
            <SelectInput source="name" />
          </ReferenceInput>
      </SimpleForm>
  </Create>
);

export default FieldCreate;


