import * as React from "react";
import { 
  Edit, 
  TextInput, 
  SimpleForm, 
  SelectInput,
  required,
  ReferenceInput
} from 'react-admin';

const FieldEdit = props => (
  <Edit title="Редагувати поле" {...props}>
      <SimpleForm>
          <TextInput disabled source="id" />
          <TextInput source="title" />
          <SelectInput 
            source="type" 
            validate={required()}
            choices={[
              {id: "str", name: "Текстовий"},
              {id: "float", name: "Числовий"},
            ]}
          />
          <ReferenceInput
              source="bot"
              reference="bots"
              validate={required()}
          >
            <SelectInput source="name" />
          </ReferenceInput>
      </SimpleForm>
  </Edit>
);

export default FieldEdit;


