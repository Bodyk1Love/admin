import * as React from "react";
import { List, Datagrid, TextField, EditButton, ReferenceField } from 'react-admin';

const FieldList = props => (
  <List {...props}>
      <Datagrid rowClick="edit">
          <TextField source="title" />
          <ReferenceField source="bot" reference="bots" label="Бот" link="show">
            <TextField source="name" />
          </ReferenceField>
          <EditButton basePath="fields" />
      </Datagrid>
  </List>
);

export default FieldList;


