import FieldCreate from './FieldCreate';
import FieldEdit from './FieldEdit';
import FieldList from './FieldList';
import TextFields from '@material-ui/icons/TextFields';

const resources = {
  icon: TextFields,
  create: FieldCreate,
  list: FieldList,
  edit: FieldEdit
};

export default resources
