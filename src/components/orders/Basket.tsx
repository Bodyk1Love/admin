import * as React from 'react';
import { FC } from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link, FieldProps, useTranslate, useQueryWithStore } from 'react-admin';

import { AppState, Order, Product } from '../../types';

const useStyles = makeStyles({
    rightAlignedCell: { textAlign: 'right' },
});

const Basket: FC<FieldProps<Order>> = ({ record }) => {
    const classes = useStyles();
    const translate = useTranslate();

    const { loaded, data: products } = useQueryWithStore<AppState>(
        {
          type: 'getMany',
          resource: 'products',
          payload: {
              ids: record ? record.items.map(item => item.product) : [],
          },
        },
        {},
        state => {
            const productIds = record
                ? record.items.map(item => item.product)
                : [];

            return productIds
                .map(
                    productId =>
                        state.admin.resources.products.data[
                            productId
                        ] as Product
                )
                .filter(r => typeof r !== 'undefined')
                .reduce((prev, next) => {
                    prev[next.id] = next;
                    return prev;
                }, {} as { [key: string]: Product });
        }
    );

    if (!loaded || !record) return null;

    return (
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>
                        {translate(
                            'resources.commands.fields.basket.reference'
                        )}
                    </TableCell>
                    <TableCell className={classes.rightAlignedCell}>
                        {translate(
                            'resources.commands.fields.basket.unit_price'
                        )}
                    </TableCell>
                    <TableCell className={classes.rightAlignedCell}>
                        {translate('resources.commands.fields.basket.quantity')}
                    </TableCell>
                    <TableCell className={classes.rightAlignedCell}>
                        {translate('resources.commands.fields.basket.total')}
                    </TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {record.items.map(
                    (item: any) =>
                        products[item.product] && (
                            <TableRow key={item.product}>
                                <TableCell>
                                    {products[item.product].title}
                                </TableCell>
                                <TableCell className={classes.rightAlignedCell}>
                                    {products[
                                        item.product
                                    ].price.toLocaleString(undefined, {
                                        style: 'currency',
                                        currency: 'USD',
                                    })}
                                </TableCell>
                                <TableCell className={classes.rightAlignedCell}>
                                    {item.quantity}
                                </TableCell>
                                <TableCell className={classes.rightAlignedCell}>
                                    {(
                                        products[item.product].price *
                                        item.quantity
                                    ).toLocaleString(undefined, {
                                        style: 'currency',
                                        currency: 'USD',
                                    })}
                                </TableCell>
                            </TableRow>
                        )
                )}
            </TableBody>
        </Table>
    );
};

export default Basket;
