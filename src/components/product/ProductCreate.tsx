import * as React from "react";
import { 
  Create, 
  TextInput, 
  SimpleForm, 
  ReferenceInput, 
  SelectInput,
  required,
  NumberInput
} from 'react-admin';
import { InputAdornment } from '@material-ui/core';


const ProductCreate = props => (
  <Create title="Створити продукт" {...props}>
      <SimpleForm>
          <TextInput source="title" validate={required()}/>
          <TextInput source="description" validate={required()}/>
          <NumberInput
              source="price"
              validate={required()}
              InputProps={{
                  startAdornment: (
                      <InputAdornment position="start">
                          ₴
                      </InputAdornment>
                  ),
              }}
          />
          <ReferenceInput
              source="bot"
              reference="bots"
              validate={required()}
          >
            <SelectInput source="name" />
          </ReferenceInput>
      </SimpleForm>
  </Create>
);

export default ProductCreate;


