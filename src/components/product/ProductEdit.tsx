import * as React from "react";
import { 
  Edit, 
  TextInput, 
  SimpleForm, 
  ReferenceInput, 
  SelectInput,
  required,
  NumberInput
} from 'react-admin';
import { InputAdornment } from '@material-ui/core';

const ProductEdit = props => (
  <Edit title="Редагувати бота" {...props}>
      <SimpleForm>
          <TextInput disabled source="id" />
          <TextInput source="title" validate={required()}/>
          <TextInput source="description" validate={required()}/>
          <NumberInput
              source="price"
              validate={required()}
              InputProps={{
                  startAdornment: (
                      <InputAdornment position="start">
                          ₴
                      </InputAdornment>
                  ),
              }}
          />
          <ReferenceInput
              source="bot"
              reference="bots"
              validate={required()}
          >
            <SelectInput source="name" />
          </ReferenceInput>
      </SimpleForm>
  </Edit>
);

export default ProductEdit;


