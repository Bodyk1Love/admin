import * as React from "react";
import { FC } from "react";

import { 
  Datagrid, 
  TextField, 
  EditButton, 
  ReferenceField, 
  FilterProps,
  ReferenceInput,
  TopToolbar,
  ListProps,
  SelectInput,
  Filter,
  ListBase,
  Title,
  SortButton,
  CreateButton,
  ExportButton,
  NumberInput,
  SearchInput,
  Pagination
} from 'react-admin';
import Aside from "./Aside"
import { Box, useMediaQuery, Theme } from '@material-ui/core';


export const ProductFilter: FC<Omit<FilterProps, 'children'>> = props => (
  <Filter {...props}>
      <SearchInput source="q" alwaysOn />
      <ReferenceInput
          source="category_id"
          reference="categories"
          sort={{ field: 'id', order: 'ASC' }}
      >
          <SelectInput source="name" />
      </ReferenceInput>
      <NumberInput source="width_gte" />
      <NumberInput source="width_lte" />
      <NumberInput source="height_gte" />
      <NumberInput source="height_lte" />
  </Filter>
);

const ListActions: FC<any> = ({ isSmall }) => (
  <TopToolbar>
      {isSmall && <ProductFilter context="button" />}
      <SortButton fields={['reference', 'sales', 'stock']} />
      <CreateButton basePath="/products" />
      <ExportButton />
  </TopToolbar>
);

const ProductList: FC<ListProps> = props => {
  const isSmall = useMediaQuery<Theme>(theme => theme.breakpoints.down('sm'));
  return (
      <ListBase
          filters={isSmall ? <ProductFilter /> : null}
          perPage={20}
          sort={{ field: 'reference', order: 'ASC' }}
          {...props}
      >
          <ProductListView isSmall={isSmall} />
      </ListBase>
  );
};

const ProductListView: FC<{ isSmall: boolean }> = ({ isSmall }) => {
  return (
      <>
          <Title defaultTitle={"Products"} />
          <ListActions isSmall={isSmall} />
          {isSmall && (
              <Box m={1}>
                  <ProductFilter context="form" />
              </Box>
          )}
          <Box display="flex">
              <Aside />
              <Box width={isSmall ? 'auto' : 'calc(100% - 16em)'}>
                  <Datagrid rowClick="edit">
                      <TextField source="title" />
                      <TextField source="description" />
                      <TextField source="price" />
                      <ReferenceField source="bot" reference="bots" label="Бот" link="show">
                        <TextField source="name" />
                      </ReferenceField>
                      <EditButton basePath="products" />
                  </Datagrid>
                  <Pagination rowsPerPageOptions={[10, 20, 40]} />
              </Box>
          </Box>
      </>
  );
};

export default ProductList;


