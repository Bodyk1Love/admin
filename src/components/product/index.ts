import ProductCreate from './ProductCreate';
import ProductList from './ProductList';
import ProductEdit from './ProductEdit';
import Fastfood from '@material-ui/icons/Fastfood';

const resources = {
  icon: Fastfood,
  create: ProductCreate,
  list: ProductList,
  edit: ProductEdit
};

export default resources
