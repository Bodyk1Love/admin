import { fetchUtils } from 'react-admin';
import { stringify } from 'query-string';

const apiUrl = 'http://localhost:8000/api/v1';
// const apiUrl = 'https://core-django.herokuapp.com/api/bot';

const httpClient = fetchUtils.fetchJson;

let setAuthHeaders = function(){
  let user = localStorage.getItem('user')
  let options = {
    headers: new Headers({ Accept: 'application/json' })
  };
  options.headers.set('Authorization', `${user}`);
  return options
}

// fetch('https://myapi/resources', {
//         method: 'POST',
//         headers: {
//             'Content-Type': 'application/json'
//         },
//         body: JSON.stringify(permissions),
//     })
//     .then(response => response.json())
//     .then(json => knownResources.filter(resource => json.resources.includes(resource.props.name)));

const dataProvider =  {
    getList: (resource, params) => {
      const { page, perPage } = params.pagination;
      const url = `${apiUrl}/${resource}/?_page=${page}&_limit=${perPage}&${stringify(params.filter)}`;
      return httpClient(url, setAuthHeaders()).then(({ headers, json }) => ({
          data: json,
          total: 100,
      }));
    },

    getOne: (resource, params) => {
      let url = `${apiUrl}/${resource}/${params.id}/`;
      return httpClient(url, setAuthHeaders()).then(({ json }) => ({
        data: json,
      }))
    },

    getMany: (resource, params) => {
        const query = {
            filter: JSON.stringify({ id: params.ids }),
        };
        const url = `${apiUrl}/${resource}/?${stringify(query)}`;
        return httpClient(url, setAuthHeaders()).then(({ json }) => ({ data: json }));
    },

    getManyReference: (resource, params) => {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const query = {
            sort: JSON.stringify([field, order]),
            range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
            filter: JSON.stringify({
                ...params.filter,
                [params.target]: params.id,
            }),
        };
        const url = `${apiUrl}/${resource}?${stringify(query)}`;

        return httpClient(url).then(({ headers, json }) => ({
            data: json,
            total: parseInt(headers.get('content-range').split('/').pop(), 10),
        }));
    },

    update: (resource, params) => {
      let url = `${apiUrl}/${resource}/${params.id}/`;
      let options = {
        method: 'PUT',
        body: JSON.stringify(params.data),
        ...setAuthHeaders()
      }
      return httpClient(url, options).then(({ json }) => ({ data: json }))
    },

    updateMany: (resource, params) => {
        const query = {
            filter: JSON.stringify({ id: params.ids}),
        };
        return httpClient(`${apiUrl}/${resource}?${stringify(query)}`, {
            method: 'PUT',
            body: JSON.stringify(params.data),
        }).then(({ json }) => ({ data: json }));
    },

    create: (resource, params) => {
      let url = `${apiUrl}/${resource}/`;
      let options = {
        method: 'POST',
        body: JSON.stringify(params.data),
        ...setAuthHeaders()
      }
      return httpClient(url, options).then(({ json }) => ({
          data: { ...params.data, id: json.id },
      }))
    },

    delete: (resource, params) => {
      let url = `${apiUrl}/${resource}/${params.id}/`;
      let options = {
        method: 'DELETE',
        ...setAuthHeaders()
      }
      return httpClient(url, options).then(({ json }) => ({ data: json }))
    },

    deleteMany: (resource, params) => {
        const query = {
            filter: JSON.stringify({ id: params.ids}),
        };
        let options = {
          method: 'DELETE',
          body: JSON.stringify(params.data),
          ...setAuthHeaders()
        }
        
        return httpClient(
          `${apiUrl}/${resource}/?${stringify(query)}`, options
        ).then(({ json }) => ({ data: json }));
    }
};

export default dataProvider